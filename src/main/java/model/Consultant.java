package model;

public class Consultant {
    private  long id;
    private String name;
    private int age;

    public Consultant() {
    }

    public Consultant(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDocument() {
        return age;
    }

    public void setDocument(int age) {
        this.age = age;
    }
}
