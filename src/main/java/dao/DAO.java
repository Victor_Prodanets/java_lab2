package dao;

import model.Car;
import model.CarShowroom;
import model.Client;
import model.Consultant;

import java.util.List;

public class DAO implements IDAO {
    DAOImpl<Car> daoCar =new DAOImpl<>(Car.class);
    DAOImpl<CarShowroom> daoCarShowroom =new DAOImpl<>(CarShowroom.class);
    DAOImpl<Client> daoClient =new DAOImpl<>(Client.class);
    DAOImpl<Consultant> daoConsultant =new DAOImpl<>(Consultant.class);

    @Override
    public Car getCar(long id) {
        return daoCar.get(id);
    }

    @Override
    public List<Car> getAllCars() {
        return daoCar.getAll();
    }

    @Override
    public Client getClient(long id) {
        return daoClient.get(id);
    }

    @Override
    public List<Client> getClients() {
        return daoClient.getAll();
    }

    @Override
    public CarShowroom getCarShowroom(long id) {
        return daoCarShowroom.get(id);
    }

    @Override
    public List<CarShowroom> getAllCarShowrooms() {
        return daoCarShowroom.getAll();
    }

    @Override
    public Consultant getConsultant(long id) {
        return daoConsultant.get(id);
    }

    @Override
    public List<Consultant> getConsultants() {
        return daoConsultant.getAll();
    }
}
