package dao;

import java.io.FileDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOImpl<T> implements IDAOImpl<T>{

    private Class<T> typeOfClass;
    private Connection connection;
    private PreparedStatement ps;

    public DAOImpl(Class<T> typeOfClass) {
        this.typeOfClass = typeOfClass;
    }



    @Override
    public T get(long id) {
        try {
            Constructor<T> constructor = typeOfClass.getConstructor();
            String className = typeOfClass.getSimpleName();
            Object obj = constructor.newInstance();
            Field[] fields = typeOfClass.getDeclaredFields();

            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","");
            String statement = String.format("select * from %ss where id=?", className.toLowerCase());
            ps = connection.prepareStatement(statement);
            ps.setInt(1, (int)id);
            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                Method method;
                for(Field field:fields){
                    String fieldName = field.getName();
                    method = typeOfClass.getMethod("set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1), field.getType());
                    method.invoke(obj, toObject(field.getType(), rs.getString(fieldName.toLowerCase())));
                }
            }

            return (T)obj;
        } catch (Exception err) {
            System.out.println(err.toString());
        }finally {
            try{
               connection.close();
            }catch (Exception err){
                System.out.println(err.toString());
            }
        }
        return null;
    }

    @Override
    public List<T> getAll() {
        try {
            Constructor<T> constructor = typeOfClass.getConstructor();
            String className = typeOfClass.getSimpleName();
            Field[] fields = typeOfClass.getDeclaredFields();

            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","");
            String statement = String.format("select * from %ss", className.toLowerCase());
            ps = connection.prepareStatement(statement);
            ResultSet rs = ps.executeQuery();

            List<T> listObj = new ArrayList<T>();

            while(rs.next()){
                Method method;
                Object obj = constructor.newInstance();
                for(Field field:fields){
                    String fieldName = field.getName();
                    method = typeOfClass.getMethod("set" + fieldName.substring(0,1).toUpperCase() + fieldName.substring(1), field.getType());
                    method.invoke(obj, toObject(field.getType(), rs.getString(fieldName.toLowerCase())));
                }
                listObj.add((T)obj);
            }

            return listObj;
        } catch (Exception err) {
            System.out.println(err.toString());
        }finally {
            try{
                connection.close();
            }catch (Exception err){
                System.out.println(err.toString());
            }
        }
        return null;
    }

    private static Object toObject(Class c, String str){
        if( Boolean.class == c || Boolean.TYPE == c){
            if (str.equals("f")) return false;
            else return true;
        }
        if( Byte.class == c || Boolean.TYPE == c) return Byte.parseByte( str );
        if( Short.class == c || Boolean.TYPE == c) return Short.parseShort( str );
        if( Integer.class == c || Boolean.TYPE == c) return Integer.parseInt( str );
        if( c.getTypeName() == "long" || Boolean.TYPE == c) return Long.parseLong( str );
        if( Float.class == c || Boolean.TYPE == c) return Float.parseFloat( str );
        if( Double.class == c || Boolean.TYPE == c) return Double.parseDouble( str );
        if ( java.util.Date.class == c || Boolean.TYPE == c) return Date.valueOf(str);
        return str;
    }
}
