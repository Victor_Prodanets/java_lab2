package dao;

import java.util.List;

public interface IDAOImpl<T> {
    T get(long id);
    List<T> getAll();
}
