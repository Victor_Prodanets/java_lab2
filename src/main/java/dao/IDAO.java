package dao;

import model.*;

import java.util.List;

public interface IDAO {
    Car getCar(long id);
    List<Car> getAllCars();

    Client getClient(long id);
    List<Client> getClients();

    CarShowroom getCarShowroom(long id);
    List<CarShowroom> getAllCarShowrooms();

    Consultant getConsultant(long id);
    List<Consultant> getConsultants();
}
