import dao.*;
import model.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DAOTests {
    DAO dao;

    @Test
    public void checkReturnCarById(){
        Car car = new Car(8, "Tesla","X");
        Car car1 = dao.getCar(8);
        assertEquals(car, car1);
    }

    @Test
    public void checkReturnAllCarsById(){
        List<Car> cars = new ArrayList<>();
        cars.add(new Car(8, "Tesla","X"));
        cars.add(new Car(1, "BMW","M3"));

        List<Car> cars1 = dao.getAllCars();
        for(int i = 0; i < cars.size(); i++){
            assertEquals(cars.get(i), cars1.get(i));
        }
    }

    @Test
    public void checkReturnCarShowroomById(){
        CarShowroom carShowroom = new CarShowroom(3, "Speed","Lenina 3");
        CarShowroom carShowroom1 = dao.getCarShowroom(3);
        assertEquals(carShowroom, carShowroom1);
    }

    @Test
    public void checkReturnAllCarShowroomsById(){
        List<CarShowroom> carShowrooms = new ArrayList<>();
        carShowrooms.add(new CarShowroom(3, "Speed","Lenina 3"));
        carShowrooms.add(new CarShowroom(1, "Car+","Pobedi 15"));

        List<CarShowroom> carShowrooms1 = dao.getAllCarShowrooms();
        for(int i = 0; i < carShowrooms.size(); i++){
            assertEquals(carShowrooms.get(i), carShowrooms1.get(i));
        }
    }

    @Test
    public void checkReturnClientById(){
        Client client = new Client(1, "Andriy","AT 145875");
        Client client1 = dao.getClient(1);
        assertEquals(client, client1);
    }

    @Test
    public void checkReturnAllClientsById(){
        List<Client> clients = new ArrayList<>();
        clients.add(new Client(1, "Andriy","AT 145875"));
        clients.add(new Client(2, "Sanya","AT 142587"));

        List<Client> clients1 = dao.getClients();
        for(int i = 0; i < clients.size(); i++){
            assertEquals(clients.get(i), clients1.get(i));
        }
    }

    @Test
    public void checkReturnConsultantById(){
        Consultant consultant = new Consultant(1, "Andriy",23);
        Consultant consultant1 = dao.getConsultant(1);
        assertEquals(consultant, consultant1);
    }

    @Test
    public void checkReturnAllConsultantsById(){
        List<Consultant> consultants = new ArrayList<>();
        consultants.add(new Consultant(1, "Andriy",23));
        consultants.add(new Consultant(1, "Sanya",52));

        List<Consultant> consultants1 = dao.getConsultants();
        for(int i = 0; i < consultants.size(); i++){
            assertEquals(consultants.get(i), consultants.get(i));
        }
    }
}
